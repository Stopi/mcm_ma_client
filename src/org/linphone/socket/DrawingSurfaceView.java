package org.linphone.socket;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import net.stopi.helpers.DisplayInformation;
import net.stopi.helpers.TransformHelper;
import net.stopi.models.Augmentation;
import net.stopi.tracking.HeadMotionTracker;
import org.linphone.R;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DrawingSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    public static int PORT = 8889;
    private DrawingThread mThread = null;
    private DisplayInformation mDisplayInfo = null;

    public DrawingSurfaceView(Context context) {
        super(context);
        init();
    }

    public DrawingSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public DrawingSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        System.out.println("[DEBUG] SurfaceView created");
        setWillNotDraw(false);
        getHolder().addCallback(this);
        mDisplayInfo = new DisplayInformation(getHeight(), getWidth());
    }

    public DrawingThread getThread() {
        return mThread;
    }

    public void onResumeDrawingSurfaceView() {
        mThread = new DrawingThread(getHolder(), mDisplayInfo);
        mThread.setRunning(true);
        mThread.start();
    }

    public void onPauseDrawingSurfaceView() {
        boolean retry = true;
        mThread.setRunning(false);
        while (retry) {
            try {
                mThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public double calculateDistance(Point p1, Point p2) {
        double a = Math.abs(p1.x - p2.x);
        double b = Math.abs(p1.y - p2.y);
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    }

    public ArrayList<Point> convertJsonToPoints(String json) {
        System.out.println("[DEBUG] JsonToPoints convert started");

        ArrayList<Point> points = new ArrayList<Point>();

        if (json.contains(",")) {
            String[] pointPairs = json.split(",");
            for (String pair : pointPairs) {
                points.add(convertCoordPairToPoint(pair));
            }
        } else {
            points.add(convertCoordPairToPoint(json));
        }

        System.out.println("[DEBUG] JsonToPoints convert finished");

        return points;
    }

    private Point convertCoordPairToPoint(String pair) {
        String[] xyPair = pair.split(";");

        int xCoord = Integer.parseInt(xyPair[0].split("=")[1]);
        int yCoord = Integer.parseInt(xyPair[1].split("=")[1]);

        return new Point(xCoord, yCoord);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Canvas c = holder.lockCanvas();
        Paint p = new Paint();
        p.setStrokeWidth(5f);
        p.setColor(Color.RED);

        c.drawColor(Color.BLACK);

        holder.unlockCanvasAndPost(c);

        onResumeDrawingSurfaceView();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        onPauseDrawingSurfaceView();
    }

    protected class DrawingThread extends Thread {

        private ServerSocket mServerSocket;
        private final SurfaceHolder mSurfaceHolder;
	    private List<Augmentation> mAugmentations;
        private boolean running = false;
        private Paint mPaint;
        private Matrix mTransform;

        protected DrawingThread(SurfaceHolder holder, DisplayInformation displayInformation) {
            mSurfaceHolder = holder;
            mPaint = new Paint();
            mPaint.setColor(Color.RED);
            mPaint.setStrokeWidth(5);
            mTransform = new Matrix();
	        mAugmentations = new ArrayList<Augmentation>();

            float translateX = 260 - (220);
            float translateY = 120 - (119);
            mTransform.setTranslate(translateX, translateY);
            mTransform.setScale(2, 2);
        }

        @Override
        public void run() {
            try {
                mServerSocket = new ServerSocket(8889);
                Log.d("Drawing", "[DEBUG] ServerThread running...");

                while (running) {
                    if (mSurfaceHolder.getSurface().isValid()) {
	                    //Log.d("Drawing", "waiting for accept");
                        Socket socket = mServerSocket.accept();
	                    //Log.d("Drawing", "accepted");
                        DataInputStream in = new DataInputStream(socket.getInputStream());
	                    //Log.d("Drawing", "before readUTF");
                        String json = in.readUTF();
	                    if (json.contains("Augmentation={"))
                            mAugmentations.add(Augmentation.fromJson(json));
	                    else if(json.contains("UpdateDrawing"))
	                    {}
	                    //Log.d("Drawing", "Augmentation added");
                        if (mAugmentations != null && !mAugmentations.isEmpty()) {

                            //Log.d("Drawing", "[DEBUG] Bitmap and Canvas preparing");

                            Canvas c = null;

                            try {

                                synchronized (mSurfaceHolder) {
                                    c = mSurfaceHolder.lockCanvas();
	                                //Log.d("Drawing", "canvas locked");
                                    doDraw(c, mAugmentations);
                                }
                            } finally {

                                //System.out.println("[DEBUG] unlockCanvasAndPost will be called");
                                if (c != null)
                                    mSurfaceHolder.unlockCanvasAndPost(c);

                            }
                        }


                    } else {
                        //System.out.println("[DEBUG] Surface is invalid");
                    }
                }
                if (mServerSocket != null && !running) {
                    mServerSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void doDraw(Canvas canvas, List<Augmentation> augmentations) {

	        //Log.d("Drawing", "in doDraw()");

            canvas.drawColor(Color.BLACK);
	        float leftBorder = 0, topBorder= 0, rightBorder = 0, bottomBorder = 0;
	        float azimuthFactor = HeadMotionTracker.getAzimuth();
	        float pitchFactor = HeadMotionTracker.getPitch();
	        //Log.d("Drawing", "Azimuth: \n" + azimuthFactor);
	        //Log.d("Drawing", "Pitch: \n" + pitchFactor);

	        Paint pp = new Paint();
	        pp.setTextAlign(Paint.Align.LEFT);
	        pp.setColor(Color.GREEN);
	        pp.setTextSize(20);
	        canvas.drawText("Azimuth: " + azimuthFactor, 0, 30, pp);
	        canvas.drawText("Pitch: " + pitchFactor, 0, 70, pp);

	        leftBorder = -300 + azimuthFactor;
	        rightBorder = 960 + azimuthFactor;
	        topBorder = -300 + pitchFactor;
	        bottomBorder = 540 + pitchFactor;

	        for(Augmentation aug : augmentations)
	        {
		        //Log.d("Drawing", "in augmentation loop");
		        float[] o= aug.getOrientation();
				List<PointF> points = aug.getPoints();
		        RectF bBox = aug.getBoundingBox();
		        //Log.d("Drawing", "bBox: \n" + bBox);
		        //Log.d("Drawing", "AzimuthAug: " + o[Augmentation.AZIMUTH]);
		        //Log.d("Drawing", "PitchAug: " + o[Augmentation.PITCH]);

		        if(leftBorder < bBox.left + o[Augmentation.AZIMUTH]
				        && topBorder < bBox.top + o[Augmentation.PITCH]
				        && rightBorder > bBox.right + o[Augmentation.AZIMUTH]
				        && bottomBorder > bBox.bottom + o[Augmentation.PITCH])
		        {

			        for (int i = 0; i <= points.size() - 2; i++)
			        {
				        PointF point1 = points.get(i);
				        PointF point2 = points.get(i + 1);

				        float[] p1 = new float[] {point1.x, point1.y};
				        float[] p2 = new float[] {point2.x, point2.y};

				        mTransform.mapPoints(p1);
				        mTransform.mapPoints(p2);

				        //Log.d("Drawing", "in point loop");

				        canvas.drawLine(
						        p1[0] + aug.getOrientation()[Augmentation.AZIMUTH] - azimuthFactor
						        , p1[1] + aug.getOrientation()[Augmentation.PITCH] - pitchFactor
						        , p2[0] + aug.getOrientation()[Augmentation.AZIMUTH] - azimuthFactor
						        , p2[1] + aug.getOrientation()[Augmentation.PITCH] - pitchFactor
						        , mPaint)
				        ;

				        //Log.d("Drawing", "[DEBUG] DrawLine from " + p1 + " to " + p2);
			        }
		        }
	        }
        }

        public boolean isRunning() {
            return running;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }
    }
}
