package org.linphone.socket;


import android.app.Activity;
import android.graphics.*;
import android.view.View;
import org.linphone.R;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class DrawingServerSocket extends Thread {
    public static int PORT = 8889;
    private Activity mActivity;
    private View mCanvasView;
    private ServerSocket mServerSocket;
    private Matrix mTransform;

    public DrawingServerSocket(Activity activity, View canvas) {
        this.mActivity = activity;
        this.mCanvasView = canvas;
        this.mTransform = new Matrix();
        mTransform.setTranslate(132, 0);
        System.out.println("[ServerSocket created]");
    }

    public void run()
    {
        try {
            mServerSocket = new ServerSocket(PORT);

            System.out.println("[DEBUG] DrawingServerSocket started");

            while(true)
            {
                Socket socket = mServerSocket.accept();
                DataInputStream in = new DataInputStream(socket.getInputStream());
                String json = in.readUTF();
                ArrayList<Point> points = convertJsonToPoints(json);
                if(points != null && !points.isEmpty() && points.size() > 2) {
                    final List<Point> pts = transformPointList(mTransform, points);
                    mCanvasView.post(new Runnable() {

                        @Override
                        public void run() {
                            Bitmap b = Bitmap.createBitmap(mCanvasView.getWidth(), mCanvasView.getHeight(), Bitmap.Config.ARGB_8888);
                            Canvas c = new Canvas(b);
                            Paint p = new Paint();
                            p.setColor(mActivity.getResources().getColor(R.color.red));
                            p.setStrokeWidth(5f);
                            p.setAntiAlias(true);
                            p.setStyle(Paint.Style.STROKE);
                            p.setStrokeJoin(Paint.Join.ROUND);

                            for (int i = 0; i < pts.size() - 2; i++) {
                                Point p1 = pts.get(i);
                                Point p2 = pts.get(i + 1);

                                if (calculateDistance(p1, p2) < 10) {
                                    c.drawLine(p1.x,
                                            p1.y,
                                            p2.x,
                                            p2.y,
                                            p);

                                    System.out.println("[DEBUG] DrawLine from " + p1 + " to " + p2);
                                }
                            }
                            mCanvasView.draw(c);
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[Exception THROWN] " + e.getMessage());
        }
    }

    public double calculateDistance(Point p1, Point p2)
    {
        double a = Math.abs(p1.x - p2.x);
        double b = Math.abs(p1.y - p2.y);
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    }

    public ArrayList<Point> convertJsonToPoints(String json)
    {
        ArrayList<Point> points = new ArrayList<Point>();

        if(json.contains(",")) {
            String[] pointPairs = json.split(",");
            for (String pair : pointPairs) {
                points.add(convertCoordPairToPoint(pair));
            }
        } else {
            points.add(convertCoordPairToPoint(json));
        }

        return points;
    }

    public Point transformPoint(Matrix transform, float[] pts)
    {
        transform.mapPoints(pts);

        return new Point((int) pts[0], (int) pts[1]);
    }

    public List<Point> transformPointList(Matrix transform, List<Point> ptsList)
    {
        List<Point> points = new ArrayList<Point>();

        for (Point p : ptsList)
        {
            float[] pts = {p.x, p.y};
            points.add(transformPoint(transform, pts));
        }

        return points;
    }

    public float[] convertPointlistToArray(List<Point> pointList)
    {
        float[] points = new float[pointList.size()*2];
        int i = 0;

        for (Point p : pointList)
        {
            points[i] = p.x;
            i++;
            points[i] = p.y;
            i++;
        }

        return points;
    }

    private Point convertCoordPairToPoint(String pair)
    {
        String[] xyPair = pair.split(";");

        int xCoord = Integer.parseInt(xyPair[0].split("=")[1]);
        int yCoord = Integer.parseInt(xyPair[1].split("=")[1]);

        return new Point(xCoord, yCoord);
    }

    public void closeSocket() throws IOException {
        if(mServerSocket != null)
            mServerSocket.close();
    }
}
