package net.stopi.models;

import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class Augmentation
{
	public static int AZIMUTH = 0;
	public static int PITCH = 1;
	public static int ROLL = 2;

	private float[] orientation;
	private List<PointF> points;
	private RectF boundingBox;

	public Augmentation(float azimuth, float pitch, float roll)
	{
		this.orientation = new float[3];
		this.orientation[AZIMUTH] = azimuth;
		this.orientation[PITCH] = pitch;
		this.orientation[ROLL] = roll;
		points = new ArrayList<PointF>();
	}

	public float[] getOrientation()
	{
		return orientation;
	}

	public void setOrientation(float[] orientation)
	{
		this.orientation = orientation;
	}

	public List<PointF> getPoints()
	{
		return points;
	}

	public void setPoints(List<PointF> points)
	{
		this.points = points;
	}

	public void addPoint(PointF point)
	{
		this.points.add(point);
	}

	public RectF getBoundingBox()
	{
		return boundingBox;
	}

	public void setBoundingBox(RectF boundingBox)
	{
		this.boundingBox = boundingBox;
	}

	public String toJson()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Augmentation={");
		sb.append(convertOrientationToString(getOrientation()));
		sb.append(convertPointListToJson(points));
		sb.append("}");

		return sb.toString();
	}

	private String convertOrientationToString(float[] orientation)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Orientation={");
		sb.append("azimuth:").append(orientation[AZIMUTH]);
		sb.append(", pitch:").append(orientation[PITCH]);
		sb.append(", roll:").append(orientation[ROLL]);
		sb.append("},");

		return sb.toString();
	}

	private String convertPointListToJson(List<PointF> points)
	{
		StringBuilder sb = new StringBuilder();

		sb.append("Points=[");

		for (PointF p : points)
		{
			sb.append("{x:").append(String.valueOf(p.x));
			sb.append(", y:").append(String.valueOf(p.y)).append("},");
		}
		sb.deleteCharAt(sb.lastIndexOf(","));

		sb.append("]");

		return sb.toString();
	}

	public static Augmentation fromJson(String json)
	{
		if (!json.contains("Augmentation={"))
			return null;

		Log.d("Extracting","in fromJson of Augmentation");
		String augmentation = json.substring(json.indexOf('{')+1, json.lastIndexOf('}'));
		Log.d("Extracting","augmentation substring created");

		float[] orientation = extractOrientation(augmentation);
		Log.d("Extracting","orientation extracted");

		List<PointF> points = extractPoints(augmentation);
		Log.d("Extracting","points extracted");

		RectF bBox = calculateBoundingBox(points);
		Log.d("Extracting","BoundingBox calculated");

		Augmentation aug = new Augmentation(orientation[AZIMUTH], orientation[PITCH], orientation[ROLL]);
		aug.setPoints(points);
		aug.setBoundingBox(bBox);
		Log.d("Extracting","Augmentation created");

		return aug;
	}

	private static List<PointF> extractPoints(String augmentation)
	{
		List<PointF> result = new ArrayList<PointF>();

		if(augmentation.contains("Points=["))
		{
			// -2 because of the last '}' that isn't followed by ','
			Log.d("Extracting", "Augmentation: \n" + augmentation);
			String pointList = augmentation.substring(augmentation.indexOf("[") + 1, augmentation.indexOf(']'));
			Log.d("Extracting", "pointList: \n" + pointList);
			//String[] points = pointList.split("},");
			List<String> points = extractPointsFromPointList(pointList);

			for(String point : points)
			{
				//Log.d("Extracting", "Point before substr: \n" + point);
				//point = point.substring(1);
				Log.d("Extracting", "Point: \n" + point);
				String[] coords = point.split(", ");

				float x = Float.valueOf(coords[0].substring(coords[0].indexOf(':') + 1));
				float y = Float.valueOf(coords[1].substring(coords[1].indexOf(':') + 1));

				result.add(new PointF(x, y));
			}
		}

		return result;
	}

	private static List<String> extractPointsFromPointList(String pointList)
	{
		List<String> points = new ArrayList<String>();

		while(pointList.contains("},"))
		{
			points.add(pointList.substring(pointList.indexOf("{") + 1, pointList.indexOf("}")));
			Log.d("Extracting", "Point substr: \n" + pointList.substring(pointList.indexOf("{") + 1, pointList.indexOf("}")));
			pointList = pointList.substring(pointList.indexOf("}") + 1);
		}
		points.add(pointList.substring(pointList.indexOf("{") + 1, pointList.indexOf("}")));
		Log.d("Extracting", "Point substr last: \n" + pointList.substring(pointList.indexOf("{") + 1, pointList.indexOf("}")));

		return points;
	}

	private static float[] extractOrientation(String augmentation)
	{
		float[] result = new float[3];

		if(augmentation.contains("Orientation={"))
		{
			String orientation = augmentation.substring(augmentation.indexOf('{') + 1, augmentation.indexOf('}') - 1);
			Log.d("extractOrientation", "at {}.");

			String[] values = orientation.split(", ");
			Log.d("extractOrientation", "at split");

			int i =0;
			for(String str : values)
			{
				String label = str.substring(0, str.indexOf(':'));
				Log.d("extractOrientation", "at label:" + label);
				float value = Float.valueOf(str.substring(str.indexOf(':')+1) );
				Log.d("extractOrientation", "at value");

				if(label.equals("azimuth"))
					result[AZIMUTH] = value;
				else if(label.equals("pitch"))
					result[PITCH] = value;
				else if(label.equals("roll"))
					result[ROLL] = value;


				Log.d("extractOrientation", "after else if");
			}
		}

		return result;
	}

	private static RectF calculateBoundingBox(List<PointF> points)
	{
		PointF ll, ur;
		RectF bBox;
		ll = new PointF(Float.MAX_VALUE, Float.MAX_VALUE);
		ur= new PointF(-1*Float.MAX_VALUE, -1*Float.MAX_VALUE);

		if (points.size() <=1)
			return null;

		float minY = Float.MAX_VALUE;
		float minX = Float.MAX_VALUE;
		float maxY = Float.MAX_VALUE*-1;
		float maxX = Float.MAX_VALUE*-1;

		for (PointF testPoint : points) {
			float y = testPoint.y;
			float x = testPoint.x;

			if(minY > y)
				minY = y;
			if(minX > x)
				minX = x;
			if(maxY < y)
				maxY = y;
			if(maxX < x)
				maxX = x;
		}

		ll.set(minX, minY);
		ur.set(maxX, maxY);

		bBox = new RectF(ll.x, ll.y, ur.x, ur.y);
		return bBox;
	}
}
