package net.stopi.tracking;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class CompassListener implements SensorEventListener {
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[HeadMotionTracker.AXIS_X];
        float y = event.values[HeadMotionTracker.AXIS_Y];
        float z = event.values[HeadMotionTracker.AXIS_Z];

        Log.d("HeadMotionCompass", "\n===========================COMPASS===========================\n");
        Log.d("HeadMotionCompass", "x = " + x + "\ny = " + y + "\nz = " + z);
        Log.d("HeadMotionCompass", "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
