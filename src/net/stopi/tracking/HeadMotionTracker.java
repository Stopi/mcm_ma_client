package net.stopi.tracking;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.util.Log;
import net.stopi.models.Augmentation;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class HeadMotionTracker implements SensorEventListener {

    public static int AXIS_X = 0;
    public static int AXIS_Y = 1;
    public static int AXIS_Z = 2;

    private Sensor mCompass;
    private Sensor mGyroscope;
    private Sensor mGravity;
    private Context mContext;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
	private Sensor mRotationVector;
    private float[] mR, mI, mAcc, mMagField, mGyro, mRotaVector, mRotationMatrix, mOri;
	private static float[] mOrientation = new float[3];
	private static int factor = 32;
	private SensorFusion mSensorFusion;

    public HeadMotionTracker(Context context) {
        mContext = context;
	    //mSensorFusion = new SensorFusion();
	    //mSensorFusion.setMode(SensorFusion.Mode.ACC_MAG);

        mR = new float[9];
        mI = new float[9];
        mAcc = new float[3];
        mMagField = new float[3];
	    mGyro = new float[3];
	    mRotaVector = new float[3];
	    mRotationMatrix = new float[9];
	    mOri = new float[3];

        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        //mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        //mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
	    mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }

    public void registerListeners(int sensorDelay)
    {
        //mSensorManager.registerListener(this, mAccelerometer, sensorDelay);
        //mSensorManager.registerListener(this, mGyroscope, sensorDelay);
        //mSensorManager.registerListener(this, mCompass, sensorDelay);
	    mSensorManager.registerListener(this, mRotationVector, sensorDelay);
    }

    public void unregisterListeners()
    {
        //mSensorManager.unregisterListener(this, mAccelerometer);
        //mSensorManager.unregisterListener(this, mCompass);
	    //mSensorManager.unregisterListener(this, mGyroscope);
	    mSensorManager.unregisterListener(this, mRotationVector);
    }

    public float[] getRotationMatrix()
    {
        if(mAcc != null && mMagField != null)
        if (SensorManager.getRotationMatrix(mR, mI, mAcc, mMagField))
            return mR;

        return null;
    }

    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType())
        {
            case Sensor.TYPE_ACCELEROMETER:
                mAcc = event.values;
	            //mSensorFusion.setAccel(mAcc);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mMagField = event.values;
	            //mSensorFusion.setMagnet(mMagField);
                break;
	        case Sensor.TYPE_GYROSCOPE:
		        mGyro = event.values;
		        //mSensorFusion.gyroFunction(event);
		        break;
	        case Sensor.TYPE_ROTATION_VECTOR:
		        mRotaVector = event.values;

		        SensorManager.getRotationMatrixFromVector(mRotationMatrix, mRotaVector);
		        SensorManager.getOrientation(mRotationMatrix, mOri);

		        Log.d("Orientation", "PrevAzimuth: " + Math.toDegrees(mOri[0]) + " |\n" +
		                             "PrevPitch: " + Math.toDegrees(mOri[1]) + " |\n" +
				                     "PrevRoll: " + Math.toDegrees(mOri[2]));

		        SensorManager.remapCoordinateSystem(mRotationMatrix
				                                    , SensorManager.AXIS_X
				                                    , SensorManager.AXIS_Z
				                                    , mRotationMatrix);

		        SensorManager.getOrientation(mRotationMatrix, mOri);

		        Log.d("Orientation", "Azimuth: " + Math.toDegrees(mOri[0]) + " |\n" +
				        "Pitch: " + Math.toDegrees(mOri[1]) + " |\n" +
				        "Roll: " + Math.toDegrees(mOri[2]));

		        mOrientation = new float[] {
				        (float) Math.toDegrees(mOri[0]) + 180
				        , (float) Math.toDegrees(mOri[1]) + 90
				        , (float) Math.toDegrees(mOri[2]) + 180
		        };

		        new RotationMatrixTask().execute(
				        new double[] {mOrientation[Augmentation.AZIMUTH]
						            , mOrientation[Augmentation.PITCH]
						            , mOrientation[Augmentation.ROLL]}
		        );

		        break;
        }

        //if(mAcc != null && mMagField != null)
            //if (SensorManager.getRotationMatrix(mR, mI, mAcc, mMagField));
                //new RotationMatrixTask().execute(mR);

		//new RotationMatrixTask().execute(
		//		new double[] {mSensorFusion.getAzimuth(), mSensorFusion.getRoll(), mSensorFusion.getPitch()}
		//);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private class RotationMatrixTask extends AsyncTask<double[], Integer, Boolean> {

        @Override
        protected Boolean doInBackground(double[]... values) {


            try {
	            // IP of Desktop
                Socket socket = new Socket("10.29.16.163", 8890);

                StringBuilder sb = new StringBuilder();
	            switch (values[0].length)
	            {
		            case 3:
			            sb.append("Orientation=[");
			            break;
		            case 9:
			            sb.append("RotationMatrix=[");
			            break;
	            }

                for (double value : values[0])
                {
                    sb.append(String.valueOf(value)).append(";");
                }
                sb.deleteCharAt(sb.length() - 1);
                sb.append("]");

                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.writeUTF(sb.toString());
                //Log.d("RotationMatrixSend", "Sending RotationMatrix to Server");
                //Log.d("RotationMatrixSend", sb.toString());

                out.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }
    }

	public static float getAzimuth()
	{
		return mOrientation[Augmentation.AZIMUTH] * factor;
	}

	public static float getPitch()
	{
		return mOrientation[Augmentation.PITCH] * factor;
	}

	public static float getRoll()
	{
		return mOrientation[Augmentation.ROLL] * factor;
	}
}
