package net.stopi.tracking;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class AccelerometerListener implements SensorEventListener {

    public void onSensorChanged(SensorEvent event) {
        float x = event.values[HeadMotionTracker.AXIS_X];
        float y = event.values[HeadMotionTracker.AXIS_Y];
        float z = event.values[HeadMotionTracker.AXIS_Z];

        Log.d("HeadMotionAccelerometer","\n=======================ACCELEROMETER======================\n");
        Log.d("HeadMotionAccelerometer", "x = " + x + "\ny = " + y + "\nz = " + z);
        Log.d("HeadMotionAccelerometer", "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
