package net.stopi.tracking.cv;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import org.linphone.mediastream.Log;

public class ColorTracker
{
	private Camera mCamera;
	public byte[] data = null;
	private static ColorTracker instance = null;

	private ColorTracker()
	{
	}

	public static ColorTracker getInstance()
	{
		if (instance == null)
			instance = new ColorTracker();

		return instance;
	}

	public void setData(byte[] value)
	{
		this.data = value.clone();
		Log.d("Image", "data set at length of " + data.length);
	}

	public byte[] getData()
	{
		return data;
	}

	public Bitmap getBitmap()
	{
		if(data != null)
		{
			Bitmap bmp;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inMutable = true;
			bmp = BitmapFactory.decodeByteArray(data, 0, data.length, options);

			return bmp;
		}

		return null;
	}
}
