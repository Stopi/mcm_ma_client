package net.stopi.helpers;

import android.graphics.Matrix;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.List;

public class TransformHelper {

    public static Point transformPoint(Matrix transform, float[] pts)
    {
        transform.mapPoints(pts);

        return new Point((int) pts[0], (int) pts[1]);
    }

    public static List<Point> transformPointList(Matrix transform, List<Point> ptsList)
    {
        List<Point> points = new ArrayList<Point>();

        for (Point p : ptsList)
        {
            float[] pts = {p.x, p.y};
            points.add(transformPoint(transform, pts));
        }

        return points;
    }
}
